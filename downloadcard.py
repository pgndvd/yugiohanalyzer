__author__ = 'davide'
import urllib2
import re
import sys

# url = "http://yugioh.wikia.com/wiki/Trap_Hole"
# url = "http://yugioh.wikia.com/wiki/Swords_of_Revealing_Light"
# url = "http://yugioh.wikia.com/wiki/Dark_Grepher"
# url = "http://yugioh.wikia.com/wiki/Relinquished"
# url = "http://yugioh.wikia.com/wiki/XYZ-Dragon_Cannon"
try:
    url = "http://yugioh.wikia.com/wiki/" + sys.argv[1].replace(" ","_")
    print(url)
    response = urllib2.urlopen(url)
    data = response.read()
    response.close()
except:
    try:
        url = "http://yugioh.wikia.com/wiki/" + sys.argv[1].replace(" ","_").replace("_(Red)","").replace("_(Blue)","").replace("_(Green)","").replace("_(Ultimate_Rare)","")
        print(url)
        response = urllib2.urlopen(url)
        data = response.read()
        response.close()
    except Exception as e:
        print(e)

monster = 1
magic = 0


##### MAGIC
##### Spell Card; Name; Normal; Card Number; Image; Effect; Effect type1; Effect type2; Effect type3; Effect type4


##### TRAP
##### Trap Card; Name; Normal; Card Number; Image; Effect; Effect type1; Effect type2; Effect type3; Effect type4



##### MONSTER
##### Monster Card; Name; DARK; warrior1; warrior2; warrior3; warrior4; Level; ATK; DEF; Card Number; Image; Effect; Effect type1; Effect type2; Effect type3; Effect type4



import datetime
tempo = datetime.datetime.now()

record = ""
################# NAME #######################
startstring = 'scope="row">English</th>'
endstring = '</td>'

startindex = str(data).index(startstring)
endindex = str(data).index(endstring,startindex)
output = data[startindex+startstring.__len__()+55:endindex]
# print(output)
################# ATTRIBUTE #######################
try:
    startstring = 'title="Attribute">Attribute</a></th>'
    endstring = ' title='
    endstring2 = '>'
    endstring3 = '</a>'

    startindex = str(data).index(startstring)
    endindex = str(data).index(endstring,startindex)
    endindex2 = str(data).index(endstring2,endindex)
    endindex3 = str(data).index(endstring3,endindex2)

    record = "Monster;"+output+";"+data[endindex2+1:endindex3]+";"
    # print("Monster Card")
    # print(data[endindex2+1:endindex3])
except:
    magic = 1
    monster = 0


################# TYPE #######################
if(monster == 1):
    startstring = 'title="Type">Type'
    endstring = '<a href="/wiki/Level"'

    startindex = str(data).index(startstring)+startstring.__len__()
    endindex = str(data).index(endstring,startindex)


    m = re.findall('">(.+?)</a', data[startindex:endindex])
    # print(m)
    try:
        record += m[0]+";"
    except:
        record += ";"
    try:
        record += m[1]+";"
    except:
        record += ";"
    try:
        record += m[2]+";"
    except:
        record += ";"
    try:
        record += m[3]+";"
    except:
        record += ";"

################# LEVEL #######################
if(monster == 1):

    startstring = '<a href="/wiki/Level"'
    endstring = '<tr id="" class="cardtablerow" style=";"><th id="" class="cardtablerowheader"'

    startindex = str(data).index(startstring)+startstring.__len__()
    endindex = str(data).index(endstring,startindex)

    m = re.findall('">(.+?)</a', data[startindex:endindex])
    # print(m)
    record += m[1]+";"

################# ATK/DEF #######################
if(monster == 1):

    startstring = '<a href="/wiki/ATK"'
    endstring = '<tr id="" class="cardtablerow" style=";"><th id="" class="cardtablerowheader"'

    startindex = str(data).index(startstring)+startstring.__len__()
    endindex = str(data).index(endstring,startindex)

    m = re.findall('">(.+?)</a', data[startindex:endindex])
    # print(m)
    record += m[2]+";"+m[3]+";"


################# TYPE #######################
if(magic == 1):

    startstring = 'scope="row">Type</th>'
    endstring = '<tr id="" class="cardtablerow" style=";"><th id="" class="cardtablerowheader"'

    startindex = str(data).index(startstring)+startstring.__len__()
    endindex = str(data).index(endstring,startindex)

    m = re.findall('">(.+?)</a', data[startindex:endindex])
    # print(m)
    record = m[0]+";"+output+";"



################# Property #######################
if(magic == 1):

    startstring = 'title="Property">Property</a></th>'
    endstring = '<tr id="" class="cardtablerow" style=";"><th id="" class="cardtablerowheader"'

    startindex = str(data).index(startstring)+startstring.__len__()
    endindex = str(data).index(endstring,startindex)

    m = re.findall('">(.+?)</a', data[startindex:endindex])
    # print(m)
    record += m[0]+";"+";"+";"+";"+";"+";"+";"+";"



################# CARD NUMBER #######################
try:
    startstring = '<a href="/wiki/Card_Number"'
    endstring = '<tr id="" class="cardtablerow" style=";"><th id="" class="cardtablerowheader"'

    startindex = str(data).index(startstring)+startstring.__len__()
    endindex = str(data).index(endstring,startindex)

    m = re.findall('">(.+?)</a', data[startindex:endindex])
    # print(m)
    record += m[1]+";"
except:
    record += ";"


################# IMAGES #######################
startstring = '<td class="cardtable-cardimage"'
endstring = '" 	class="'

startindex = str(data).index(startstring)
endindex = str(data).index(endstring,startindex)

# print(data[startindex+startstring.__len__()+23:endindex])
record += data[startindex+startstring.__len__()+23:endindex]+";"



################# Card effect #######################
startstring = '<tr><th class="navbox-title"><div style="font-size: 110%;"><span style="font-style: italic;">English</span></div></th></tr>'
startstring2 = '<tr><td class="navbox-list" style="text-align: left; width: 100%; padding: 0px; border: none;">'
endstring = '</table>'

startindex = str(data).index(startstring)
startindex2 = str(data).index(startstring2,startindex) + startstring2.__len__()
endindex = str(data).index(endstring,startindex)

m = re.sub('<a(.+?)>',"", data[startindex2+1:endindex-15]).replace("</a>","")
# print(m)
record += m.replace(";",",").replace("\"","")+";"



################# Card effect types #######################
try:
    startstring = 'scope="row">Card effect types</th>'
    endstring = 'Card descriptions'

    startindex = str(data).index(startstring)
    endindex = str(data).index(endstring,startindex)

    m = re.findall('">(.+?)</a', data[startindex:endindex])
    # print(m)
    for item in m:
        record+=item+";"
except:
    record+=";"



print(record)



with open("test.csv", "a") as myfile:
    myfile.write(record+'\n')



# print(datetime.datetime.now()-tempo)