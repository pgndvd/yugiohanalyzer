__author__ = 'Davide Pagano'
import random
import re



# CHANGE THE GRAMMAR HERE
dict = {'S': '(0A|1B1B|ε)',
        'A': '(0AA|1S)',
        'B': '(1BB|0S)'};



def generate_strings(rule):
    # print(rule)
    if not rule:
        return [""]
    begin,end=rule[0],rule[1:]
    if begin=='[':
        i=end.find(']')
        if i==-1:
            raise Exception("Unmatched '['")
        alt,end=end[0:i],end[i+1:]
        return [a+e for e in generate_strings(end) for a in [alt,""]]
    if begin=='(':
        i=end.find(')')
        if i==-1:
            raise Exception("Unmatched '('")
        alt,end=end[0:i].split('|'),end[i+1:]
        return [a+e for e in generate_strings(end) for a in alt]
    if begin in [']',')','|']:
        raise Exception("Unexpected " + begin)
    return [begin + e for e in generate_strings(end)]



nonterminal = 'S'
i = 1
print("S -> substituing "+ nonterminal, end=""),
final = "S"
while (i<10):

    # Generate list of possible derivations
    rule = dict[nonterminal]
    derivations = generate_strings(rule)
    # Randomly choses one possible derivation
    derivation = random.choice(derivations)

    # Replaces all the occurence of the
    # nonterminal with the derivation chosen
    final = final.replace(nonterminal,derivation)
    print(" into "+derivation+" -> "+ final)

    # List the nonterminals
    nonterminallist = re.findall('[A-Z][^A-Z]*', final)

    # No more nonterminals
    if (nonterminallist == []):
        print ("final string: " +final.replace("ε",""))
        print ("# of 0: "+str(final.count("0")))
        print ("# of 1: "+str(final.count("1")))
        exit()


    # Pick one random nonterminal from the list
    nonterminal = random.choice(nonterminallist)[0]
    print(final+" -> substituing "+ nonterminal,end=""),

    i=i+1










